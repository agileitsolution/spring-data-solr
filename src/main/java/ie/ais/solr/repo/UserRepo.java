package ie.ais.solr.repo;

import ie.ais.solr.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.solr.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<User, Long>
{

		@Query("name:?0")
		public User findByUserName(String name);

}
