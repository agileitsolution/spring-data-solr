package ie.ais.solr.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.data.solr.repository.*;

import ie.ais.solr.entity.Book;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface BookRepo extends SolrCrudRepository<Book, Long>
{

		@Query("title:?0")
		public Book findByBookTitle(String name);

        Page<Book> findByPopularity(Integer popularity, Pageable page);

        Page<Book> findByNameOrDescription(@Boost(2) String name, String description, Pageable page);

        @Highlight
        @Query(fields = {"name"}, // "title", "description"},
           defaultOperator = org.springframework.data.solr.core.query.Query.Operator.AND
        )
		HighlightPage<Book> findByNameIn(Collection<String> names, Pageable page);

        @Query(value = "name:?0")
        @Facet(fields = { "name" }, limit=20)
		FacetPage<Book> findByNameAndFacetOnCategory(String name, Pageable page);


}
