package ie.ais.solr.entity;


import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

public class User implements Serializable
{

		private static final long serialVersionUID = -8243145429438016231L;
		
		@Id
		@Field
		private String id;
		
		@Field
		private String sf_name;

		@Field
		private String status;

		@Field
		private String user_first_name;

		@Field
		private String user_name;

		@Field
		private String	user_last_name;

		@Field
		private String	type;

		@Field
		private String	sf_status;

		@Field
		private String	name;

		@Field
		private String	usr_grp_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSf_name() {
        return sf_name;
    }

    public void setSf_name(String sf_name) {
        this.sf_name = sf_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_first_name() {
        return user_first_name;
    }

    public void setUser_first_name(String user_first_name) {
        this.user_first_name = user_first_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_last_name() {
        return user_last_name;
    }

    public void setUser_last_name(String user_last_name) {
        this.user_last_name = user_last_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSf_status() {
        return sf_status;
    }

    public void setSf_status(String sf_status) {
        this.sf_status = sf_status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsr_grp_name() {
        return usr_grp_name;
    }

    public void setUsr_grp_name(String usr_grp_name) {
        this.usr_grp_name = usr_grp_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!getId().equals(user.getId())) return false;
        if (!getSf_name().equals(user.getSf_name())) return false;
        if (getStatus() != null ? !getStatus().equals(user.getStatus()) : user.getStatus() != null) return false;
        if (getUser_first_name() != null ? !getUser_first_name().equals(user.getUser_first_name()) : user.getUser_first_name() != null)
            return false;
        if (getUser_name() != null ? !getUser_name().equals(user.getUser_name()) : user.getUser_name() != null)
            return false;
        if (getUser_last_name() != null ? !getUser_last_name().equals(user.getUser_last_name()) : user.getUser_last_name() != null)
            return false;
        if (!getType().equals(user.getType())) return false;
        if (!getSf_status().equals(user.getSf_status())) return false;
        if (!getName().equals(user.getName())) return false;
        return getUsr_grp_name().equals(user.getUsr_grp_name());
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getSf_name().hashCode();
        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        result = 31 * result + (getUser_first_name() != null ? getUser_first_name().hashCode() : 0);
        result = 31 * result + (getUser_name() != null ? getUser_name().hashCode() : 0);
        result = 31 * result + (getUser_last_name() != null ? getUser_last_name().hashCode() : 0);
        result = 31 * result + getType().hashCode();
        result = 31 * result + getSf_status().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getUsr_grp_name().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", sf_name='" + sf_name + '\'' +
                ", status='" + status + '\'' +
                ", user_first_name='" + user_first_name + '\'' +
                ", user_name='" + user_name + '\'' +
                ", user_last_name='" + user_last_name + '\'' +
                ", type='" + type + '\'' +
                ", sf_status='" + sf_status + '\'' +
                ", name='" + name + '\'' +
                ", usr_grp_name='" + usr_grp_name + '\'' +
                '}';
    }

}
