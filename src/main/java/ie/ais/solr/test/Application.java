package ie.ais.solr.test;


import ie.ais.solr.entity.User;
import ie.ais.solr.repo.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import ie.ais.solr.entity.Book;
import ie.ais.solr.repo.BookRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.SolrPageRequest;
import org.springframework.data.solr.core.query.result.HighlightPage;

import java.util.Arrays;

public class Application
{

		private static Logger LOG = LoggerFactory.getLogger(Application.class);

		public static void main(String[] args)
		{


				ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new ClassPathResource("spring-config.xml").getPath());
				BookRepo bookRepo = context.getBean(BookRepo.class);
                UserRepo userRepo = context.getBean(UserRepo.class);

				Book hobbit = new Book();
				hobbit.setId("3");
				hobbit.setTitle("Hobbit");
				hobbit.setName("Hobbit");
				hobbit.setPopularity(13);
				hobbit.setDescription("Prelude to LOTR");
				bookRepo.save(hobbit);

				User user = getUser();
                userRepo.save(user);


				LOG.info("BOOK:: {}", bookRepo.findOne(3l));
				LOG.info("BOOK:: {}", bookRepo.findByBookTitle("Hobbit"));
				Pageable pagable  = new SolrPageRequest(1, 20);
                LOG.info("BOOK:: {}", bookRepo.findByNameAndFacetOnCategory("Hobbit",pagable));

			    HighlightPage<Book> rezult1 = bookRepo.findByNameIn(Arrays.asList("Hobbit"), pagable );
				LOG.info("BOOK:: {}"  + rezult1.getHighlighted().size());
//			    LOG.info("BOOK:: {}", rezult1.iterator().next());

                Page<Book> rezult2 = bookRepo.findByPopularity(13, pagable );
                LOG.info("BOOK:: {}"  + rezult2.getTotalElements());
			    LOG.info("BOOK:: {}", rezult2.getContent());

                LOG.info("User:: {}", userRepo.findOne(1l));
                LOG.info("User:: {}", userRepo.findByUserName("name"));
				context.close();

		}

        private static User getUser() {
		    User user = new User();
		    user.setId("1");
		    user.setName("name");
		    user.setSf_name("sf_name");
            user.setSf_status("1");
            user.setStatus("1");
            user.setType("type");
            user.setUser_first_name("first name");
            user.setUser_last_name("last name");
            user.setUsr_grp_name("grp name");
            user.setUser_name("user name");
            return user;
        }
}
